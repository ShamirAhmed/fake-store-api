import React from "react";
import './css/ShowData.css';
import Filter from "./Filter";
import Header from "./Header";
import Products from "./Products";
import { BrowserRouter as Routers, Route, Routes } from 'react-router-dom';
import Params from "./Params";
import AddProduct from "./AddProduct";

class ShowData extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: this.props.data,
            filterPanal: false,
            checkBoxRadio: ''
        }
    }

    openFilter = () => {
        this.setState({
            filterPanal: !this.state.filterPanal
        })
    }

    check = (event) => {
        if (event.target.checked) {
            const filteredData = this.props.data.filter((each) => {
                return each.category === event.target.id
            });
            this.setState({
                data: filteredData,
                checkBoxRadio: {
                    [event.target.id]: event.target.checked
                }
            })
        } else {
            this.setState({
                data: this.props.data,
                checkBoxRadio: {
                    [event.target.id]: event.target.checked
                }
            })
        }
    }
    render() {
        console.log(this.state.data);
        return (
            <>
                <Routers>
                    <Header openFilter={this.openFilter} />
                    <Filter check={this.check} show={this.state.filterPanal} checked={this.state.checkBoxRadio} />
                    <Routes>
                        <Route path="/" element={<Products data={this.props.data} />} />
                        <Route path="/product/:id" element={<Params />} />
                        <Route path="/addProduct" element={<AddProduct addProduct={this.props.addProduct} />} />
                    </Routes>
                </Routers>
            </>
        );
    }
}

export default ShowData;