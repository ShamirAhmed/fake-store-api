import React from "react";
import './css/Products.css';
import {Link} from "react-router-dom";
// import Params from "./Params";

class Products extends React.Component {
    render() {
        console.log('Product render')
        return (
            <>
                {/* <Routers > */}
                    <div className="noteworthy">
                        <div className="table-holder">
                            {
                                Array.isArray(this.props.data)
                                    ?
                                    this.props.data
                                        .map((each, id) => {
                                            // console.log(each);
                                            return (
                                                <Link to={`/product/${each.id}`} className="table" key={id}>
                                                    <h3>{each.title}</h3>
                                                    <img src={each.image} alt="product" />
                                                    <span className="price">${each.price}</span><br />
                                                    <span className="rating">Rating {}</span>
                                                </Link>
                                            );
                                        })

                                    :
                                    <>
                                        <div className="table" key={this.props.data.id}>
                                            <h3>{this.props.data.title}</h3>
                                            <img src={this.props.data.image} alt="product" />
                                            <span className="price">${this.props.data.price}</span><br />
                                            <span className="rating">Rating {this.props.data.rating.rate}</span>
                                            <div>
                                                <p>{this.props.data.description}</p>
                                            </div>
                                        </div>
                                    </>
                            }
                        </div>
                    </div>
                {/* </Routers> */}
            </>
        );
    }
}

export default Products;