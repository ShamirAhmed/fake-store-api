import React from "react";
import './css/AddProduct.css';
import Input from "./Input";
import validator from 'validator';

class AddProduct extends React.Component {
    constructor(){
        super();
        this.state = {
            title: '',
            image: '',
            category: '',
            description: '',
            price: '',  
            rating: '',
            titleError: '',
            imageError: '',
            categoryError: '',
            descriptionError: '',
            priceError: '',
            ratingError: '',
        }
    }

    change = (event) => {
        this.setState({
            [event.target.id]: event.target.value
        })
    }

    validate = () => {
        let flag = true;

        if(!validator.isAlpha(this.state.title)){
            this.setState({
                titleError: 'Please Enter Valid Title'
            })
            flag = false;
        } else {
            this.setState({
                titleError: ''
            })
        }

        if(!validator.isURL(this.state.image)){
            this.setState({
                imageError: 'Please Enter Valid image URL'
            })
            flag = false;
        } else {
            this.setState({
                imageError: ''
            })
        }

        if(!validator.isAlpha(this.state.category)) {
            this.setState({
                categoryError: 'Please Enter Valid Category'
            })
            flag = false;
        } else {
            this.setState({
                categoryError: ''
            })
        }

        if(!validator.isAlpha(this.state.description)) {
            this.setState({
                descriptionError: 'Please Enter Valid Description'
            })
            flag = false;
        } else {
            this.setState({
                descriptionError: ''
            })
        }

        if(!validator.isNumeric(this.state.price)) {
            this.setState({
                priceError: 'Please Enter Valid Price'
            })
            flag = false;
        } else {
            this.setState({
                priceError: ''
            })
        }

        if(!validator.isNumeric(this.state.rating)) {
            this.setState({
                ratingError: 'Please Enter Valid Rating'
            })
            flag = false;
        } else {
            this.setState({
                ratingError: ''
            })
        }

        if(flag === true){
            this.props.addProduct(this.state);
        }
    }

    render() {
        return (
            <>
                <h1 className="addProduct-h1">Add Product</h1>
                <div className="form">
                    {/*
                        title , image, catagory, discription, price, rating
                    */}
                    <Input change={this.change} label='title' warning={this.state.titleError} />
                    <Input change={this.change} label='image' warning={this.state.imageError} />
                    <Input change={this.change} label='category' warning={this.state.categoryError}/>
                    <Input change={this.change} label='description' warning={this.state.descriptionError}/>
                    <Input change={this.change} label='price' warning={this.state.priceError}/>
                    <Input change={this.change} label='rating' warning={this.state.ratingError}/>
                    <div>
                        <button onClick={this.validate}>Submit</button>
                    </div>
                </div>
            </>
        )
    }
}

export default AddProduct;