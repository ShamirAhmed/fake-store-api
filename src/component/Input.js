import React, { Component } from 'react'

export default class Input extends Component {
    render() {
        return (
            <>
                <div>
                    <label>{this.props.label.toUpperCase()}</label>
                    <input type='text' id={this.props.label} onChange={this.props.change}/>
                    <span className='warning'>{this.props.warning.length > 0 ? this.props.warning : <>&nbsp;</>}</span>
                </div>
            </>
        )
    }
}
