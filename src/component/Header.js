import React from "react";
import { Link } from "react-router-dom";
import './css/Header.css';

class Header extends React.Component {
    render() {
        return (
            <>
                <header>
                    <span className="logo">Fake Store API</span>
                    <div className="header-right">
                        <Link to='/'>
                            <span className="active" >Home</span>
                        </Link>
                        <span onClick={this.props.openFilter}>Filter</span>
                        <Link to='/addProduct'>
                            <span>Add Product</span>
                        </Link>
                    </div>
                </header>
            </>
        );
    }
}

export default Header;