import { useParams } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import Loader from './Loader'
import Axios from 'axios';
import './css/Params.css'

const Params = () => {

    const pageStatus = {
        loding: 'loding',
        loaded: 'loaded',
        error: 'error',
    }
    const params = useParams()
    const { id } = params

    const [productDetails, setProductDetails] = useState([])
    const [isLoading, setIsLoading] = useState(pageStatus.loding)

    useEffect(() => {
        Axios.get(`https://fakestoreapi.com/products/${id}`)
            .then((data) => {
                setProductDetails(data.data)
                setIsLoading(pageStatus.loaded)
                console.log(data.data)
            })
            .catch((err) => {
                setIsLoading(pageStatus.error)
            });
    }, {})


    return (
        <>
            {isLoading === pageStatus.loding ?
                <>
                    <Loader />
                </>
                :
                undefined
            }

            {isLoading === pageStatus.error ?
                <>
                    <h1>Product Not Found</h1>
                </>
                :
                undefined
            }

            {isLoading === pageStatus.loaded ?
                <>
                    <div className="table">
                        <h3>{productDetails.title}</h3>
                        <img src={productDetails.image} alt="product" />
                        <span className="price">${productDetails.price}</span><br />
                        <span className="rating">Rating {productDetails.rating.rate}</span>
                        <div>
                            <p>{productDetails.description}</p>
                        </div>
                    </div>

                </>
                :
                undefined
            }
        </>
    )
}

export default Params;