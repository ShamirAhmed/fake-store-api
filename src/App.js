import './App.css';
import React from 'react';
import Loader from './component/Loader';
import Axios from 'axios';
import Error from './component/Error';
import ShowData from './component/ShowData';

class App extends React.Component {

  constructor() {
    super();

    this.state = {
      apiData: 'loading',
    }
  }

  addProduct = (data) => {
    if(Array.isArray(this.state.apiData)) {
      console.log('yes');
      this.setState({
        apiData: [...this.state.apiData].concat(data)
      },() => {
        console.log(this.state.apiData)
      });
    } else {
      console.log("no");
    }
  }

  componentDidMount() {
    console.log('mount');
    Axios.get('https://fakestoreapi.com/products')
      .then((data) => {
        this.setState({
          apiData: data.data,
        })
        console.log(data.data)
      })
      .catch((err) => {
        this.setState({
          apiData: 'error',
        })
      });
  }

  render() {
    console.log('render')
    return (
      <>
        {
          this.state.apiData === 'loading'
          &&
          <Loader />}
        {
          (this.state.apiData === 'error' || this.state.apiData.length === 0)
          &&
          <Error error="Something Went Wrong" />
        }
        {
          this.state.apiData !== 'loading'
          &&
          this.state.apiData !== 'error'
          &&
          this.state.apiData.length !== 0
          &&
          <ShowData data={this.state.apiData} addProduct={this.addProduct} />
        }
      </>
    );
  }
}

export default App;
