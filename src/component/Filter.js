import React from "react";
import './css/Filter.css';

class Filter extends React.Component {
    render() {
        // console.log(this.props.checked)
        return (
            <>
                <div className={`filter-panal${this.props.show ? '' : ' displayNone'}`} >
                    <div className="box">
                        <div>
                            <label htmlFor="jewelery">Jewelerys</label>
                            <input id='jewelery' name="filter" checked={this.props.checked.jewelery} type='checkbox' onChange={this.props.check} />
                        </div>
                        <div>
                            <label htmlFor="electronics">Electronics</label>
                            <input id='electronics' name="filter" checked={this.props.checked.electronics} type='checkbox' onChange={this.props.check} />
                        </div>
                        <div>
                            <label htmlFor="men's clothing">men's clothing</label>
                            <input id="men's clothing" name="filter" checked={this.props.checked["men's clothing"]} type='checkbox' onChange={this.props.check} />
                        </div>
                        <div>
                            <label htmlFor="women's clothing">women's clothing</label>
                            <input id="women's clothing" name="filter" checked={this.props.checked["women's clothing"]} type='checkbox' onChange={this.props.check} />
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Filter;